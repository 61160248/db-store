/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.db.store.POC;

import Model.Customer;
import Model.Product;
import Model.Receipt;
import Model.User;

/**
 *
 * @author user
 */
public class TestReceipt {
    public static void main(String[] args) {
        Product p1 = new Product(1,"Coco",30);
        Product p2 = new Product(2, "Chayen", 30);
        User seller = new User("Num","1234567890","pass");
        Customer cus = new Customer("Pim","0987654321");
        Receipt receipt = new Receipt(seller,cus);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 2);
        System.out.println(receipt);
    }
}
